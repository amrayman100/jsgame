var level = 1;
function getGuaranteed(id) {
    var el = document.getElementById(id);
    if (el == null) {
        throw new Error("Element #" + id + " not found.");
    }
    return el;
}
function start() {
    var left = getGuaranteed("left");
    var right = getGuaranteed("right");
    var i;
    var turn = Math.floor((Math.random() * level - 1) + 1);
    console.log(turn);
    for (i = 0; i < level; i++) {
        var pic = Math.round(Math.random());
        var src = "cherry.png";
        if (pic) {
            src = "grape.png";
        }
        var x = Math.floor((Math.random() * (30)));
        var y = Math.floor((Math.random() * (500 - 100)));
        var cherryleft = document.createElement('img');
        var cherryright = document.createElement('img');
        cherryleft.className = 'fruiticon';
        cherryleft.src = './' + src;
        cherryleft.style.position = "relative";
        cherryleft.style.left = x + "px";
        cherryleft.style.top = y + "px";
        cherryright.className = 'fruiticon';
        cherryright.src = './' + src;
        cherryright.style.position = "relative";
        cherryright.style.left = x + "px";
        cherryright.style.top = y + "px";
        cherryleft.addEventListener("click", gameOver);
        right.appendChild(cherryright);
        left.appendChild(cherryleft);
        if (i == turn) {
            pic = Math.floor((Math.random() * level - 1) + 1);
            var src = "cherry.png";
            if (pic) {
                src = "grape.png";
            }
            var x1 = Math.floor((Math.random() * (200)));
            var y1 = Math.floor((Math.random() * (600 - 100)));
            var cherryGoal = document.createElement('img');
            cherryGoal.className = 'fruiticon';
            cherryGoal.src = './' + src;
            cherryGoal.style.position = "absolute";
            cherryGoal.style.left = x1 + "px";
            cherryGoal.style.top = y1 + "px";
            cherryGoal.addEventListener("click", generate);
            left.appendChild(cherryGoal);
        }
    }
}
function generate() {
    var snd = new Audio('./eat.wav');
    snd.play();
    level++;
    var left = getGuaranteed("left");
    var right = getGuaranteed("right");
    var child = left.lastElementChild;
    while (child) {
        left.removeChild(child);
        child = left.lastElementChild;
    }
    var child = right.lastElementChild;
    while (child) {
        right.removeChild(child);
        child = right.lastElementChild;
    }
    start();
}
function gameOver() {
    alert("Game Over Loser");
    var snd = new Audio('./gameover.wav');
    snd.play();
    level = 1;
    var left = getGuaranteed("left");
    var right = getGuaranteed("right");
    var child = left.lastElementChild;
    while (child) {
        left.removeChild(child);
        child = left.lastElementChild;
    }
    var child = right.lastElementChild;
    while (child) {
        right.removeChild(child);
        child = right.lastElementChild;
    }
    start();
}
$(function () {
    start();
});
